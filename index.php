<?php
// Lab work 1.2.1.  Work in the console OpenServer
$wor = "Word";
echo "Hello $wor"; // Usind echo with " and variable. Outputs:  "Hello Word
# New line separator


echo PHP_EOL;
echo  'Hello $wor', PHP_EOL; // Usind echo with ' and variable. Outputs: "Hello $wor"
/*Print new string using function print*/
print ("Hello $wor");
echo PHP_EOL;
print ('Hello $wor');
echo PHP_EOL;
echo PHP_EOL;
/* Kill all next executions and exit the application */
// die('Lab work 1.2.1. end');

echo "1.2.2 Data types  test *******************************************\n";
echo $xx = 8 - 6.4;  echo PHP_EOL;
echo $yy = 1.6;  echo PHP_EOL;
//ob_start();
var_dump($xx);  // Rasulr   is   float(1.6)
//ob_end_clean();
var_dump($xx==$yy);  // Result is  bool(false)
echo PHP_EOL;
echo '(0.1 + 0.7)*10 = '; echo (0.1 + 0.7)*10;  // Resalt is 8
echo PHP_EOL;
echo '(int)((0.1 + 0.7)*10) = '; echo (int)((0.1 + 0.7)*10);  // Resalt is 7
//*****************************************************************************************************
echo PHP_EOL;
echo PHP_EOL;


echo "Lab 1.2.2.1 *******************************************\n";
$integer =  105;
$float =  20.25;
$string = "Hello!";
$boolean = true;

echo "\$int is integer= $integer\n"; // It is integer variable. Result :  0
var_dump($integer);
echo '$flo is float='.$float."\n"; // It is float variable. Result :  20.25
var_dump($float);
echo '$str'." is string= $string \n"; // It is string variable
var_dump($string);
echo '$flo'." is boolean= $boolean \n";  //It is boolean variable
var_dump($float);
